On Error Resume Next

'Plugin Nagios - contador de usuarios logueados en servidor, si hay logueados alarma y los muestra
' nsc.ini
' check_users_connect_audit=cscript.exe -T:120 -NoLogo scripts\check_users_connect_audit.vbs localhost
' Ruta en nagios de los archivos por samba : /opt/nagios/etc/smbpublico/check_users_connect

If Wscript.Arguments.Count < 1 Then 
    Wscript.Echo "Ejecute el comando: check_users_connect_audit.vbs <servidoroip>"
    Wscript.Echo "Ejemplo: cscript -NoLogo check_users_connect_audit.vbs localhost"
	Wscript.Quit
End If

'**************************************************************
'**************************************************************

'Const for return val's
Const intOK       = 0
Const intWarning  = 1
Const intCritical = 2
Const intUnknown  = 3
Const ForReading = 1

servidor = WScript.Arguments(0)

'**************************************************************
'* Variables
	
Const warning = 0
Const critical = 0

strRutaEjecutableInfoExtendida="c:\windows\system32\TSSessionNfo.exe"
strRutaCarpeta="\\clo519.tecnoquimicas.com\servidores\check_users_connect\"
strRutaExcepciones_infraestructura = strRutaCarpeta & "check_users_connect_list_excluidos_infraestructura.txt"

	'**************************************************************
	'* Formatear usuarios excluidos del equipo en particular
	Set objShell_hostname = CreateObject("WScript.Shell")	
	Set objWshScriptExec_hostname = objShell_hostname.Exec("%WINDIR%\system32\hostname.exe")
	Set objStdOut_hostname = objWshScriptExec_hostname.StdOut
	While Not objStdOut_hostname.AtEndOfStream
		strLine = objStdOut_hostname.ReadLine
	Wend
	
	strRutaExcepciones_hostname = strRutaCarpeta & "check_users_connect_list_excluidos_" & strLine & ".txt"
	'WScript.Echo strRutaExcepciones_hostname 

	
'**************************************************************
'* Chequeo de existencia de archivos
	Set objFSO = CreateObject("Scripting.FileSystemObject")

	If Not objFSO.FileExists(strRutaEjecutableInfoExtendida) Then
		sendMessage = "UNKNOWN: Error opening: " & Replace(strRutaEjecutableInfoExtendida,"\","\\")
		WScript.Echo sendMessage
		Wscript.Quit(intUnknown)
	End If
	
	If Not objFSO.FileExists(strRutaExcepciones_infraestructura) Then	
		sendMessage = "UNKNOWN: Error opening: " & Replace(strRutaExcepciones_infraestructura,"\","\\")
		WScript.Echo sendMessage
		Wscript.Quit(intUnknown)
	End If

	If Not objFSO.FileExists(strRutaExcepciones_hostname) Then	
		sendMessage = "UNKNOWN: Error opening: " & Replace(strRutaExcepciones_hostname,"\","\\")
		WScript.Echo sendMessage
		Wscript.Quit(intUnknown)
	End If
	
'**************************************************************
'* chequeo de conexion de usuarios

strComando = "%WINDIR%\system32\query.exe user /server:" & servidor
intNumeroDeLineasInicialesAIgnorar=1
intNumeroDeColumnasResultado=8
intUmvralMinutosInactividad=30
Dim ArrayResultado(8)

Set objShell = CreateObject("WScript.Shell")
' saca solo 1 linea de un archivo
Set objWshScriptExec = objShell.Exec(strComando)
Set objStdOut = objWshScriptExec.StdOut

contador = 1
strUsuarios=""
strConsola=""
intUsuarios=0
 While Not objStdOut.AtEndOfStream
	If contador > intNumeroDeLineasInicialesAIgnorar Then
		strLine = objStdOut.ReadLine
		'Eliminando usuario identificado de session que hace la consulta
		strfiltrado = replace(strLine,">","")
		'Eliminando espacios al inicio y al final
		strfiltrado = LTrim(strfiltrado)
		strfiltrado = RTrim(strfiltrado)
		'strfiltrado = replace(strfiltrado," ",",")
		'Wscript.echo strfiltrado
		'Separando columnas de resultados, creando variables de las cadenas que no son espacios
		strfiltrado_array = Split(strfiltrado, " ")
		'strfiltrado_array = Filter (strfiltrado_array, "")
		
		contador_columnas=1
		strArrayResultado=""
		For i = 0 to Ubound(strfiltrado_array) 
				If Not StrComp(strfiltrado_array(i),"",vbTextCompare) = 0 Then
					If contador_columnas < intNumeroDeColumnasResultado Then 
						ArrayResultado(contador_columnas) = strfiltrado_array(i)						
					End If
						contador_columnas = contador_columnas + 1
				End If
		Next
		'Imprimiendo los resultados
		' Wscript.Echo "Usuario=" & ArrayResultado(1) & ", sesion=" & ArrayResultado(3) & ", IDLE TIME=" & ArrayResultado(5)
		strUsuarios= strUsuarios & ArrayResultado(1) 
		If IsNumeric(ArrayResultado(3)) Then
			strConsola= strConsola & ArrayResultado(3) 
		Else
			strConsola= strConsola & ArrayResultado(2) 
		End If
				
		intUsuarios=intUsuarios+1
		If Not objStdOut.AtEndOfStream Then
			strUsuarios= strUsuarios & ","
			strConsola= strConsola & ","
		End If
	Else
		strLine = objStdOut.ReadLine	
	End If
	contador = contador + 1
Wend

Function IdentificarDatosSesion(strServidor,intID)
	Set objShell = CreateObject("WScript.Shell")
	' saca solo 1 linea de un archivo
	Set objWshScriptExec = objShell.Exec(strRutaEjecutableInfoExtendida & " * /server:" & strServidor)
	Set objStdOut = objWshScriptExec.StdOut

	' Imprime linea por linea
	While Not objStdOut.AtEndOfStream
		strID = -1
		strUser=""
		strPC=""
		strIP=""
		strLine = objStdOut.ReadLine
		strLine = LTrim(strLine)
		strLine = RTrim(strLine)
		If InStr(strLine,"TS Session ID") > 0 Then
			strsplit = Split(strLine, ":")		
			strID=LTrim(strsplit(1))
		End If
		If strID >= 0 Then
			strLine = objStdOut.ReadLine
			strLine = LTrim(strLine)
			strLine = RTrim(strLine)
			If InStr(strLine,"TS Client Username Name") > 0 Then
				strsplit = Split(strLine, ":")
				strUser=LTrim(strsplit(1))
			End If
			strdato= StrComp(strUser,"")
			
			If StrComp(strUser,"") Then
				strLine = objStdOut.ReadLine
				strLine = objStdOut.ReadLine
				strLine = LTrim(strLine)
				strLine = RTrim(strLine)
				If InStr(strLine,"TS Client Name") > 0 Then
					strsplit = Split(strLine, ":")
					strPC=LTrim(strsplit(1))
				End If
				strLine = objStdOut.ReadLine
				strLine = LTrim(strLine)
				strLine = RTrim(strLine)
				If InStr(strLine,"TS Client IP") > 0 Then
					strsplit = Split(strLine, ":")
					strIP=LTrim(strsplit(1))
				End If
			End If
		End If
		' Si hay nombre de usuario, existe el dato		
		If StrComp(strUser,"") Then
			If intID = CInt(strID) Then
				' wscript.echo "ID=" & strID & ",User=" & strUser & ",PC=" & strPC & ",IP=" & strIP
				' strReturn = "ID=" & strID & ",User=" & strUser & ",PC=" & strPC & ",IP=" & strIP
				strReturn = "PC=" & strPC & ",IP=" & strIP
				IdentificarDatosSesion = strReturn
			End If
		End If
	Wend	
End Function

if intUsuarios >= 1 Then
	' wscript.echo "Numero Usuarios Logueados (" & servidor & ")=" & intUsuarios
	'wscript.echo "Usuarios=" & strUsuarios
	arrUsuarios = split (strUsuarios,",")
	
	'**************************************************************
	'* Formatear usuarios excluidos de infraestructura

	If objFSO.FileExists(strRutaExcepciones_infraestructura) Then	
		Set objFile = objFSO.OpenTextFile(strRutaExcepciones_infraestructura, ForReading)
		Do Until objFile.AtEndOfStream
			linea = objFile.ReadLine
			For i = 0 to UBound(arrUsuarios)
				If StrComp(arrUsuarios(i),linea,vbTextCompare) = 0 then
					arrUsuarios(i)=arrUsuarios(i) & "(Excluido)"
				End If
			Next
		Loop
		objFile.Close
	Else
		sendMessage = "UNKNOWN: Error opening: " & Replace(strRutaExcepciones_infraestructura,"\","\\")
		WScript.Echo sendMessage
		Wscript.Quit(intUnknown)
	End If

	'**************************************************************
	'* Formatear usuarios excluidos de hostname

	If objFSO.FileExists(strRutaExcepciones_hostname) Then	
		Set objFile = objFSO.OpenTextFile(strRutaExcepciones_hostname, ForReading)
		Do Until objFile.AtEndOfStream
			linea = objFile.ReadLine
			For i = 0 to UBound(arrUsuarios)
				If StrComp(arrUsuarios(i),linea,vbTextCompare) = 0 then
					arrUsuarios(i)=arrUsuarios(i) & "(Excluido)"
				End If
			Next
		Loop
		objFile.Close
	Else
		sendMessage = "UNKNOWN: Error opening: " & Replace(strRutaExcepciones_hostname,"\","\\")
		WScript.Echo sendMessage
		Wscript.Quit(intUnknown)
	End If

	'**************************************************************
	'* Si hay usuarios no excluidos conectados se reporta
	intUsuariosNoExcluidos=0
	strUsuarios=""
	For i = 0 To UBound(arrUsuarios)
		If Instr(1, arrUsuarios(i), "(Excluido)") = 0 Then
			intUsuariosNoExcluidos=intUsuariosNoExcluidos+1
		End If
		If i = UBound(arrUsuarios) Then
			strUsuarios= strUsuarios & arrUsuarios(i)
		Else
			strUsuarios= strUsuarios & arrUsuarios(i) & ","
		End If
	Next	
	' wscript.echo "Numero Usuarios Logueados (" & servidor & ")=" & intUsuarios
	' wscript.echo "Numero Usuarios No Excluidos=" & intUsuariosNoExcluidos
	' wscript.echo strUsuarios
	
	' Marcando datos adicionales como placa e ip, para cada sesion de usuarios
	arrConsola = split (strConsola,",")
	strUsuariosExtendido = split (strUsuarios,",")
	strUsuarios = ""
	For i = 0 To UBound(arrConsola)
		If i = UBound(arrConsola) Then
			strUsuarios = strUsuarios & "{" & strUsuariosExtendido(i) & "," & IdentificarDatosSesion(servidor,CInt(arrConsola(i))) & "}"
		Else
			strUsuarios = strUsuarios & "{" & strUsuariosExtendido(i) & "," & IdentificarDatosSesion(servidor,CInt(arrConsola(i))) & "}" & ","
		End If
	Next
Else
	wscript.echo "Numero Usuarios Logueados (" & servidor & ")=" & intUsuarios
End If



	
'**************************************************************
'**************************************************************
	
If intUsuarios = 0 Then
	intUsuariosNoExcluidos = 0
Else
	strUsuarios = ", " & strUsuarios
End If


If intUsuariosNoExcluidos > warning Then
	sendMessage = "WARNING: Usuarios Logueados [" & intUsuarios & "], Sin Autorizacion [" & intUsuariosNoExcluidos & "]" & strUsuarios & " | 'Usuarios Logueados'=" & intUsuarios & ";" & warning & ";" & critical & ";0;" & intUsuarios
	WScript.Echo sendMessage
	Wscript.Quit(intWarning)
Else
	sendMessage = "OK: Usuarios Logueados [" & intUsuarios & "], Sin Autorizacion [" & intUsuariosNoExcluidos & "]" & strUsuarios & " | 'Usuarios Logueados'=" & intUsuarios & ";" & warning & ";" & critical & ";0;" & intUsuarios
	WScript.Echo sendMessage
	Wscript.Quit(intOK)
End If


'Si llega hasta aqui, no se conoce el error
sendMessage = "UNKNOWN: Error en plugin"
WScript.Echo sendMessage
Wscript.Quit(intUnknown)

