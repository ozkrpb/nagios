--
-- Table structure for table `tbl_command`
--

DROP TABLE IF EXISTS `tbl_command`;
CREATE TABLE `tbl_command` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `command_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `command_line` text COLLATE utf8_unicode_ci NOT NULL,
  `command_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(10) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_name` (`command_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_command`
--

LOCK TABLES `tbl_command` WRITE;
INSERT INTO `tbl_command` VALUES (1,'notify-host-by-email','/usr/bin/printf \"%b\" \"***** Nagios *****\\n\\nNotification Type: $NOTIFICATIONTYPE$\\nHost: $HOSTNAME$\\nState: $HOSTSTATE$\\nAddress: $HOSTADDRESS$\\nInfo: $HOSTOUTPUT$\\n\\nDate/Time: $LONGDATETIME$\\n\" | /usr/bin/mail -s \"** $NOTIFICATIONTYPE$ Host Alert: $HOSTNAME$ is $HOSTSTATE$ **\" $CONTACTEMAIL$',2,'1','1','2019-05-15 22:20:30',0,1),(2,'notify-service-by-email','/usr/bin/printf \"%b\" \"***** Nagios *****\\n\\nNotification Type: $NOTIFICATIONTYPE$\\n\\nService: $SERVICEDESC$\\nHost: $HOSTALIAS$\\nAddress: $HOSTADDRESS$\\nState: $SERVICESTATE$\\n\\nDate/Time: $LONGDATETIME$\\n\\nAdditional Info:\\n\\n$SERVICEOUTPUT$\\n\" | /usr/bin/mail -s \"** $NOTIFICATIONTYPE$ Service Alert: $HOSTALIAS$/$SERVICEDESC$ is $SERVICESTATE$ **\" $CONTACTEMAIL$',2,'1','1','2019-05-15 22:20:30',0,1),(3,'check-host-alive','$USER1$/check_ping -4 -H $HOSTADDRESS$ -w 3000.0,80% -c 5000.0,100% -p 5',1,'1','1','2019-05-15 22:40:21',0,1),(4,'check_local_disk','$USER1$/check_disk -w $ARG1$ -c $ARG2$ -p $ARG3$',1,'1','1','2019-05-15 22:20:30',0,1),(5,'check_local_load','$USER1$/check_load -w $ARG1$ -c $ARG2$',1,'1','1','2019-05-15 22:20:30',0,1),(6,'check_local_procs','$USER1$/check_procs -w $ARG1$ -c $ARG2$ -s $ARG3$',1,'1','1','2019-05-15 22:20:30',0,1),(7,'check_local_users','$USER1$/check_users -w $ARG1$ -c $ARG2$',1,'1','1','2019-05-15 22:20:30',0,1),(8,'check_local_swap','$USER1$/check_swap -w $ARG1$ -c $ARG2$',1,'1','1','2019-05-15 22:20:30',0,1),(9,'check_local_mrtgtraf','$USER1$/check_mrtgtraf -F $ARG1$ -a $ARG2$ -w $ARG3$ -c $ARG4$ -e $ARG5$',1,'1','1','2019-05-15 22:20:30',0,1),(10,'check_ftp','$USER1$/check_ftp -H $HOSTADDRESS$ $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(11,'check_hpjd','$USER1$/check_hpjd -H $HOSTADDRESS$ $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(12,'check_snmp','$USER1$/check_snmp -H $HOSTADDRESS$ $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(13,'check_http','$USER1$/check_http -I $HOSTADDRESS$ $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(14,'check_ssh','$USER1$/check_ssh $ARG1$ $HOSTADDRESS$',1,'1','1','2019-05-15 22:20:30',0,1),(15,'check_dhcp','$USER1$/check_dhcp $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(16,'check_ping','$USER1$/check_ping -H $HOSTADDRESS$ -w $ARG1$ -c $ARG2$ -p 5 -4',1,'1','1','2019-05-15 22:53:11',0,1),(17,'check_pop','$USER1$/check_pop -H $HOSTADDRESS$ $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(18,'check_imap','$USER1$/check_imap -H $HOSTADDRESS$ $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(19,'check_smtp','$USER1$/check_smtp -H $HOSTADDRESS$ $ARG1$',1,'1','1','2019-05-15 22:20:30',0,1),(20,'check_tcp','$USER1$/check_tcp -H $HOSTADDRESS$ -p $ARG1$ $ARG2$',1,'1','1','2019-05-15 22:20:30',0,1),(21,'check_udp','$USER1$/check_udp -H $HOSTADDRESS$ -p $ARG1$ $ARG2$',1,'1','1','2019-05-15 22:20:30',0,1),(22,'check_nt','$USER1$/check_nt -H $HOSTADDRESS$ -p 12489 -v $ARG1$ $ARG2$',1,'1','1','2019-05-15 22:20:30',0,1),(23,'process-host-perfdata','/usr/bin/printf \"%b\" \"$LASTHOSTCHECK$\\t$HOSTNAME$\\t$HOSTSTATE$\\t$HOSTATTEMPT$\\t$HOSTSTATETYPE$\\t$HOSTEXECUTIONTIME$\\t$HOSTOUTPUT$\\t$HOSTPERFDATA$\\n\" >> /opt/nagios/var/host-perfdata.out',2,'1','1','2019-05-15 22:20:30',0,1),(24,'process-service-perfdata','/usr/bin/printf \"%b\" \"$LASTSERVICECHECK$\\t$HOSTNAME$\\t$SERVICEDESC$\\t$SERVICESTATE$\\t$SERVICEATTEMPT$\\t$SERVICESTATETYPE$\\t$SERVICEEXECUTIONTIME$\\t$SERVICELATENCY$\\t$SERVICEOUTPUT$\\t$SERVICEPERFDATA$\\n\" >> /opt/nagios/var/service-perfdata.out',2,'1','1','2019-05-15 22:20:30',0,1),(25,'process-service-perfdata-file','/bin/mv /opt/pnp4nagios/var/service-perfdata /opt/pnp4nagios/var/spool/service-perfdata.$TIMET$',2,'1','1','2019-05-15 22:20:30',0,1),(26,'process-host-perfdata-file','/bin/mv /opt/pnp4nagios/var/host-perfdata /opt/pnp4nagios/var/spool/host-perfdata.$TIMET$',2,'1','1','2019-05-15 22:20:30',0,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
CREATE TABLE `tbl_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactgroups` int(10) unsigned NOT NULL DEFAULT '0',
  `contactgroups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `minimum_importance` int(11) DEFAULT NULL,
  `host_notifications_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `service_notifications_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `host_notification_period` int(10) unsigned NOT NULL DEFAULT '0',
  `service_notification_period` int(10) unsigned NOT NULL DEFAULT '0',
  `host_notification_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `service_notification_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `host_notification_commands` int(10) unsigned NOT NULL DEFAULT '0',
  `host_notification_commands_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `service_notification_commands` int(10) unsigned NOT NULL DEFAULT '0',
  `service_notification_commands_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `can_submit_commands` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_status_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_nonstatus_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pager` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_variables` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(10) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_name` (`contact_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_contact`
--

LOCK TABLES `tbl_contact` WRITE;
INSERT INTO `tbl_contact` VALUES (1,'nagiosadmin','Nagios Admin',0,2,NULL,2,2,0,0,'','',0,2,0,2,2,2,2,'nagios@localhost','','','','','','','','',0,1,2,'1','1','2019-05-15 22:34:01',0,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_contactgroup`
--

DROP TABLE IF EXISTS `tbl_contactgroup`;
CREATE TABLE `tbl_contactgroup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contactgroup_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `members` int(10) unsigned NOT NULL DEFAULT '0',
  `contactgroup_members` int(10) unsigned NOT NULL DEFAULT '0',
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(10) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_name` (`contactgroup_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_contactgroup`
--

LOCK TABLES `tbl_contactgroup` WRITE;
INSERT INTO `tbl_contactgroup` VALUES (1,'admins','Nagios Administrators',1,0,'1','1','2019-05-15 22:34:01',0,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_contacttemplate`
--

DROP TABLE IF EXISTS `tbl_contacttemplate`;
CREATE TABLE `tbl_contacttemplate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactgroups` int(10) unsigned NOT NULL DEFAULT '0',
  `contactgroups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `minimum_importance` int(11) DEFAULT NULL,
  `host_notifications_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `service_notifications_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `host_notification_period` int(11) NOT NULL DEFAULT '0',
  `service_notification_period` int(11) NOT NULL DEFAULT '0',
  `host_notification_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `service_notification_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `host_notification_commands` int(10) unsigned NOT NULL DEFAULT '0',
  `host_notification_commands_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `service_notification_commands` int(10) unsigned NOT NULL DEFAULT '0',
  `service_notification_commands_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `can_submit_commands` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_status_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_nonstatus_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pager` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_variables` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(10) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_name` (`template_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_contacttemplate`
--

LOCK TABLES `tbl_contacttemplate` WRITE;
INSERT INTO `tbl_contacttemplate` VALUES (1,'generic-contact','',0,2,NULL,2,2,1,1,'d,u,r,f,s','w,u,c,r,f,s',1,2,1,2,2,2,2,'','','','','','','','',0,0,2,'0','1','2019-05-15 22:23:16',0,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_host`
--

DROP TABLE IF EXISTS `tbl_host`;
CREATE TABLE `tbl_host` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `host_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parents` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `parents_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `importance` int(11) DEFAULT NULL,
  `hostgroups` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hostgroups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_command` text COLLATE utf8_unicode_ci NOT NULL,
  `use_template` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `initial_state` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `max_check_attempts` int(11) DEFAULT NULL,
  `check_interval` int(11) DEFAULT NULL,
  `retry_interval` int(11) DEFAULT NULL,
  `active_checks_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `passive_checks_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_period` int(11) NOT NULL DEFAULT '0',
  `obsess_over_host` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_freshness` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `freshness_threshold` int(11) DEFAULT NULL,
  `event_handler` int(11) NOT NULL DEFAULT '0',
  `event_handler_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `low_flap_threshold` int(11) DEFAULT NULL,
  `high_flap_threshold` int(11) DEFAULT NULL,
  `flap_detection_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `flap_detection_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `process_perf_data` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_status_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_nonstatus_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `contacts` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `contacts_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `contact_groups` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `contact_groups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `notification_interval` int(11) DEFAULT NULL,
  `notification_period` int(11) NOT NULL DEFAULT '0',
  `first_notification_delay` int(11) DEFAULT NULL,
  `notification_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notifications_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `stalking_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `icon_image_alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vrml_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statusmap_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `2d_coords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `3d_coords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_variables` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(10) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_name` (`host_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_host`
--

LOCK TABLES `tbl_host` WRITE;
INSERT INTO `tbl_host` VALUES (1,'google.com','google.com','','google.com',0,2,NULL,0,2,'3',1,2,'',NULL,NULL,NULL,2,2,0,2,2,NULL,0,2,NULL,NULL,2,'',2,2,2,0,2,0,2,NULL,0,NULL,'',2,'','','','','','','','','','',0,'','1','1','2019-05-15 22:38:26',0,1),(2,'localhost','localhost','','127.0.0.1',0,2,NULL,0,2,'',1,2,'',NULL,NULL,NULL,2,2,0,2,2,NULL,0,2,NULL,NULL,2,'',2,2,2,0,2,0,2,NULL,0,NULL,'',2,'','','','','','','','','','',0,'','1','1','2019-05-15 23:09:17',0,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_hosttemplate`
--

DROP TABLE IF EXISTS `tbl_hosttemplate`;
CREATE TABLE `tbl_hosttemplate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parents` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `parents_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `importance` int(11) DEFAULT NULL,
  `hostgroups` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hostgroups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_command` text COLLATE utf8_unicode_ci NOT NULL,
  `use_template` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `initial_state` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `max_check_attempts` int(11) DEFAULT NULL,
  `check_interval` int(11) DEFAULT NULL,
  `retry_interval` int(11) DEFAULT NULL,
  `active_checks_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `passive_checks_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_period` int(11) NOT NULL DEFAULT '0',
  `obsess_over_host` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_freshness` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `freshness_threshold` int(11) DEFAULT NULL,
  `event_handler` int(11) NOT NULL DEFAULT '0',
  `event_handler_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `low_flap_threshold` int(11) DEFAULT NULL,
  `high_flap_threshold` int(11) DEFAULT NULL,
  `flap_detection_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `flap_detection_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `process_perf_data` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_status_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_nonstatus_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `contacts` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `contacts_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `contact_groups` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `contact_groups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `notification_interval` int(11) DEFAULT NULL,
  `notification_period` int(11) NOT NULL DEFAULT '0',
  `first_notification_delay` int(11) DEFAULT NULL,
  `notification_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notifications_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `stalking_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `icon_image_alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vrml_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statusmap_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `2d_coords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `3d_coords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_variables` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(8) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_name` (`template_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_hosttemplate`
--

LOCK TABLES `tbl_hosttemplate` WRITE;
INSERT INTO `tbl_hosttemplate` VALUES (1,'generic-host','',0,2,NULL,0,2,'0',0,2,'',NULL,NULL,NULL,2,2,0,2,2,NULL,0,1,NULL,NULL,1,'',1,1,1,0,2,0,2,NULL,1,NULL,'',1,'','','','/pnp4nagios/graph?host=$HOSTNAME$','','','','','','',0,'0','1','2019-05-15 22:59:53',0,1),(2,'linux-server','',0,2,NULL,0,2,'3',1,2,'',10,5,1,2,2,1,2,2,NULL,0,2,NULL,NULL,2,'',2,2,2,0,2,1,2,120,2,NULL,'d,u,r',2,'','','','','','','','','','',0,'0','1','2019-05-15 22:23:16',0,1),(3,'windows-server','',0,2,NULL,1,2,'3',1,2,'',10,5,1,2,2,1,2,2,NULL,0,2,NULL,NULL,2,'',2,2,2,0,2,1,2,30,1,NULL,'d,r',2,'','','','','','','','','','',0,'0','1','2019-05-15 22:23:16',0,1),(4,'generic-printer','',0,2,NULL,0,2,'3',1,2,'',10,5,1,2,2,1,2,2,NULL,0,2,NULL,NULL,2,'',2,2,2,0,2,1,2,30,2,NULL,'d,r',2,'','','','','','','','','','',0,'0','1','2019-05-15 22:23:16',0,1),(5,'generic-switch','',0,2,NULL,0,2,'3',1,2,'',10,5,1,2,2,1,2,2,NULL,0,2,NULL,NULL,2,'',2,2,2,0,2,1,2,30,1,NULL,'d,r',2,'','','','','','','','','','',0,'0','1','2019-05-15 22:23:16',0,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkContactToContacttemplate`
--

DROP TABLE IF EXISTS `tbl_lnkContactToContacttemplate`;
CREATE TABLE `tbl_lnkContactToContacttemplate` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `idSort` int(11) NOT NULL,
  `idTable` tinyint(4) NOT NULL,
  PRIMARY KEY (`idMaster`,`idSlave`,`idTable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkContactToContacttemplate`
--

LOCK TABLES `tbl_lnkContactToContacttemplate` WRITE;
INSERT INTO `tbl_lnkContactToContacttemplate` VALUES (1,1,1,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkContactToVariabledefinition`
--

DROP TABLE IF EXISTS `tbl_lnkContactToVariabledefinition`;
CREATE TABLE `tbl_lnkContactToVariabledefinition` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkContactToVariabledefinition`
--

LOCK TABLES `tbl_lnkContactToVariabledefinition` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkContactgroupToContact`
--

DROP TABLE IF EXISTS `tbl_lnkContactgroupToContact`;
CREATE TABLE `tbl_lnkContactgroupToContact` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkContactgroupToContact`
--

LOCK TABLES `tbl_lnkContactgroupToContact` WRITE;
INSERT INTO `tbl_lnkContactgroupToContact` VALUES (1,1,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkContacttemplateToCommandHost`
--

DROP TABLE IF EXISTS `tbl_lnkContacttemplateToCommandHost`;
CREATE TABLE `tbl_lnkContacttemplateToCommandHost` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkContacttemplateToCommandHost`
--

LOCK TABLES `tbl_lnkContacttemplateToCommandHost` WRITE;
INSERT INTO `tbl_lnkContacttemplateToCommandHost` VALUES (1,1,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkContacttemplateToCommandService`
--

DROP TABLE IF EXISTS `tbl_lnkContacttemplateToCommandService`;
CREATE TABLE `tbl_lnkContacttemplateToCommandService` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkContacttemplateToCommandService`
--

LOCK TABLES `tbl_lnkContacttemplateToCommandService` WRITE;
INSERT INTO `tbl_lnkContacttemplateToCommandService` VALUES (1,2,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkHostToHosttemplate`
--

DROP TABLE IF EXISTS `tbl_lnkHostToHosttemplate`;
CREATE TABLE `tbl_lnkHostToHosttemplate` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `idSort` int(11) NOT NULL,
  `idTable` tinyint(4) NOT NULL,
  PRIMARY KEY (`idMaster`,`idSlave`,`idTable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkHostToHosttemplate`
--

LOCK TABLES `tbl_lnkHostToHosttemplate` WRITE;
INSERT INTO `tbl_lnkHostToHosttemplate` VALUES (1,2,2,1),(1,1,1,1),(2,2,1,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkHostgroupToHost`
--

DROP TABLE IF EXISTS `tbl_lnkHostgroupToHost`;
CREATE TABLE `tbl_lnkHostgroupToHost` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkHostgroupToHost`
--

LOCK TABLES `tbl_lnkHostgroupToHost` WRITE;
INSERT INTO `tbl_lnkHostgroupToHost` VALUES (2,2,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkHosttemplateToContactgroup`
--

DROP TABLE IF EXISTS `tbl_lnkHosttemplateToContactgroup`;
CREATE TABLE `tbl_lnkHosttemplateToContactgroup` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkHosttemplateToContactgroup`
--

LOCK TABLES `tbl_lnkHosttemplateToContactgroup` WRITE;
INSERT INTO `tbl_lnkHosttemplateToContactgroup` VALUES (2,1,0),(3,1,0),(4,1,0),(5,1,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkHosttemplateToHostgroup`
--

DROP TABLE IF EXISTS `tbl_lnkHosttemplateToHostgroup`;
CREATE TABLE `tbl_lnkHosttemplateToHostgroup` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkHosttemplateToHostgroup`
--

LOCK TABLES `tbl_lnkHosttemplateToHostgroup` WRITE;
INSERT INTO `tbl_lnkHosttemplateToHostgroup` VALUES (3,1,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkHosttemplateToHosttemplate`
--

DROP TABLE IF EXISTS `tbl_lnkHosttemplateToHosttemplate`;
CREATE TABLE `tbl_lnkHosttemplateToHosttemplate` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `idSort` int(11) NOT NULL,
  `idTable` tinyint(4) NOT NULL,
  PRIMARY KEY (`idMaster`,`idSlave`,`idTable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkHosttemplateToHosttemplate`
--

LOCK TABLES `tbl_lnkHosttemplateToHosttemplate` WRITE;
INSERT INTO `tbl_lnkHosttemplateToHosttemplate` VALUES (2,1,1,1),(3,1,1,1),(4,1,1,1),(5,1,1,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkServiceToHost`
--

DROP TABLE IF EXISTS `tbl_lnkServiceToHost`;
CREATE TABLE `tbl_lnkServiceToHost` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkServiceToHost`
--

LOCK TABLES `tbl_lnkServiceToHost` WRITE;
INSERT INTO `tbl_lnkServiceToHost` VALUES (3,2,0),(4,2,0),(5,2,0),(6,2,0),(8,2,0),(9,2,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkServiceToServicetemplate`
--

DROP TABLE IF EXISTS `tbl_lnkServiceToServicetemplate`;
CREATE TABLE `tbl_lnkServiceToServicetemplate` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `idSort` int(11) NOT NULL,
  `idTable` tinyint(4) NOT NULL,
  PRIMARY KEY (`idMaster`,`idSlave`,`idTable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkServiceToServicetemplate`
--

LOCK TABLES `tbl_lnkServiceToServicetemplate` WRITE;
INSERT INTO `tbl_lnkServiceToServicetemplate` VALUES (2,2,1,1),(3,2,1,1),(4,2,1,1),(5,2,1,1),(6,2,1,1),(8,2,1,1),(9,2,1,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkServicetemplateToContactgroup`
--

DROP TABLE IF EXISTS `tbl_lnkServicetemplateToContactgroup`;
CREATE TABLE `tbl_lnkServicetemplateToContactgroup` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkServicetemplateToContactgroup`
--

LOCK TABLES `tbl_lnkServicetemplateToContactgroup` WRITE;
INSERT INTO `tbl_lnkServicetemplateToContactgroup` VALUES (1,1,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkServicetemplateToServicetemplate`
--

DROP TABLE IF EXISTS `tbl_lnkServicetemplateToServicetemplate`;
CREATE TABLE `tbl_lnkServicetemplateToServicetemplate` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `idSort` int(11) NOT NULL,
  `idTable` tinyint(4) NOT NULL,
  PRIMARY KEY (`idMaster`,`idSlave`,`idTable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkServicetemplateToServicetemplate`
--

LOCK TABLES `tbl_lnkServicetemplateToServicetemplate` WRITE;
INSERT INTO `tbl_lnkServicetemplateToServicetemplate` VALUES (2,1,1,1);
UNLOCK TABLES;

--
-- Table structure for table `tbl_lnkTimeperiodToTimeperiodUse`
--

DROP TABLE IF EXISTS `tbl_lnkTimeperiodToTimeperiodUse`;
CREATE TABLE `tbl_lnkTimeperiodToTimeperiodUse` (
  `idMaster` int(11) NOT NULL,
  `idSlave` int(11) NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMaster`,`idSlave`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lnkTimeperiodToTimeperiodUse`
--

LOCK TABLES `tbl_lnkTimeperiodToTimeperiodUse` WRITE;
INSERT INTO `tbl_lnkTimeperiodToTimeperiodUse` VALUES (5,4,0);
UNLOCK TABLES;

--
-- Table structure for table `tbl_logbook`
--

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `mnuId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mnuTopId` int(10) unsigned NOT NULL,
  `mnuGrpId` int(10) unsigned NOT NULL DEFAULT '0',
  `mnuCntId` int(10) unsigned NOT NULL,
  `mnuName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mnuLink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mnuActive` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `mnuOrderId` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`mnuId`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_menu`
--

LOCK TABLES `tbl_menu` WRITE;
INSERT INTO `tbl_menu` VALUES (1,0,0,1,'Main page','admin.php',1,1),(2,0,0,1,'Supervision','admin/monitoring.php',1,2),(3,0,0,1,'Alarming','admin/alarming.php',1,3),(4,0,0,1,'Commands','admin/commands.php',1,4),(5,0,0,1,'Specialties','admin/specials.php',1,5),(6,0,0,1,'Tools','admin/tools.php',1,6),(7,0,0,1,'Administration','admin/administration.php',1,7),(8,2,0,1,'Host','admin/hosts.php',1,1),(9,2,0,1,'Services','admin/services.php',1,2),(10,2,0,1,'Host groups','admin/hostgroups.php',1,3),(11,2,0,1,'Service groups','admin/servicegroups.php',1,4),(12,2,0,1,'Host templates','admin/hosttemplates.php',1,5),(13,2,0,1,'Service templates','admin/servicetemplates.php',1,6),(14,3,0,1,'Contact data','admin/contacts.php',1,1),(15,3,0,1,'Contact groups','admin/contactgroups.php',1,2),(16,3,0,1,'Time periods','admin/timeperiods.php',1,3),(17,3,0,1,'Contact templates','admin/contacttemplates.php',1,4),(18,4,0,1,'Definitions','admin/checkcommands.php',1,1),(19,5,0,1,'Host dependency','admin/hostdependencies.php',1,1),(20,5,0,1,'Host escalation','admin/hostescalations.php',1,2),(21,5,0,1,'Extended Host','admin/hostextinfo.php',1,3),(22,5,0,1,'Service dependency','admin/servicedependencies.php',1,4),(23,5,0,1,'Service escalation','admin/serviceescalations.php',1,5),(24,5,0,1,'Extended Service','admin/serviceextinfo.php',1,6),(25,6,0,1,'Data import','admin/import.php',1,1),(26,6,0,1,'Delete backup files','admin/delbackup.php',1,2),(27,6,0,1,'Delete config files','admin/delconfig.php',1,3),(28,6,0,1,'Nagios config','admin/nagioscfg.php',1,4),(29,6,0,1,'CGI config','admin/cgicfg.php',1,5),(30,6,0,1,'Nagios control','admin/verify.php',1,6),(31,7,0,1,'New password','admin/password.php',1,1),(32,7,0,1,'User admin','admin/user.php',1,2),(33,7,0,1,'Group admin','admin/group.php',1,3),(34,7,0,1,'Menu access','admin/menuaccess.php',1,4),(35,7,0,1,'Data domains','admin/datadomain.php',1,5),(36,7,0,1,'Config targets','admin/configtargets.php',1,6),(37,7,0,1,'Logbook','admin/logbook.php',1,7),(38,7,0,1,'Settings','admin/settings.php',1,8),(39,7,0,1,'Help editor','admin/helpedit.php',1,9),(40,7,0,1,'Support','admin/support.php',1,10);
UNLOCK TABLES;

--
-- Table structure for table `tbl_relationinformation`
--

DROP TABLE IF EXISTS `tbl_relationinformation`;
CREATE TABLE `tbl_relationinformation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tableName1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tableName2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkTable` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `targetKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullRelation` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `flags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_relationinformation`
--

LOCK TABLES `tbl_relationinformation` WRITE;
INSERT INTO `tbl_relationinformation` VALUES (1,'tbl_timeperiod','tbl_timeperiod','','exclude','tbl_lnkTimeperiodToTimeperiod','timeperiod_name','','',0,'',2),(2,'tbl_contact','tbl_command','','host_notification_commands','tbl_lnkContactToCommandHost','command_name','','',0,'',2),(3,'tbl_contact','tbl_command','','service_notification_commands','tbl_lnkContactToCommandService','command_name','','',0,'',2),(4,'tbl_contact','tbl_contactgroup','','contactgroups','tbl_lnkContactToContactgroup','contactgroup_name','','',0,'',2),(5,'tbl_contact','tbl_timeperiod','','host_notification_period','','timeperiod_name','','',0,'',1),(6,'tbl_contact','tbl_timeperiod','','service_notification_period','','timeperiod_name','','',0,'',1),(7,'tbl_contact','tbl_contacttemplate','tbl_contact','use_template','tbl_lnkContactToContacttemplate','template_name','name','',0,'',3),(8,'tbl_contact','tbl_variabledefinition','','use_variables','tbl_lnkContactToVariabledefinition','name','','',0,'',4),(9,'tbl_contacttemplate','tbl_command','','host_notification_commands','tbl_lnkContacttemplateToCommandHost','command_name','','',0,'',2),(10,'tbl_contacttemplate','tbl_command','','service_notification_commands','tbl_lnkContacttemplateToCommandService','command_name','','',0,'',2),(11,'tbl_contacttemplate','tbl_contactgroup','','contactgroups','tbl_lnkContacttemplateToContactgroup','contactgroup_name','','',0,'',2),(12,'tbl_contacttemplate','tbl_timeperiod','','host_notification_period','','timeperiod_name','','',0,'',1),(13,'tbl_contacttemplate','tbl_timeperiod','','service_notification_period','','timeperiod_name','','',0,'',1),(14,'tbl_contacttemplate','tbl_contacttemplate','tbl_contact','use_template','tbl_lnkContacttemplateToContacttemplate','template_name','name','',0,'',3),(15,'tbl_contacttemplate','tbl_variabledefinition','','use_variables','tbl_lnkContacttemplateToVariabledefinition','name','','',0,'',4),(16,'tbl_contactgroup','tbl_contact','','members','tbl_lnkContactgroupToContact','contact_name','','',0,'',2),(17,'tbl_contactgroup','tbl_contactgroup','','contactgroup_members','tbl_lnkContactgroupToContactgroup','contactgroup_name','','',0,'',2),(18,'tbl_hosttemplate','tbl_host','','parents','tbl_lnkHosttemplateToHost','host_name','','',0,'',2),(19,'tbl_hosttemplate','tbl_hostgroup','','hostgroups','tbl_lnkHosttemplateToHostgroup','hostgroup_name','','',0,'',2),(20,'tbl_hosttemplate','tbl_contactgroup','','contact_groups','tbl_lnkHosttemplateToContactgroup','contactgroup_name','','',0,'',2),(21,'tbl_hosttemplate','tbl_contact','','contacts','tbl_lnkHosttemplateToContact','contact_name','','',0,'',2),(22,'tbl_hosttemplate','tbl_timeperiod','','check_period','','timeperiod_name','','',0,'',1),(23,'tbl_hosttemplate','tbl_command','','check_command','','command_name','','',0,'',1),(24,'tbl_hosttemplate','tbl_timeperiod','','notification_period','','timeperiod_name','','',0,'',1),(25,'tbl_hosttemplate','tbl_command','','event_handler','','command_name','','',0,'',1),(26,'tbl_hosttemplate','tbl_hosttemplate','tbl_host','use_template','tbl_lnkHosttemplateToHosttemplate','template_name','name','',0,'',3),(27,'tbl_hosttemplate','tbl_variabledefinition','','use_variables','tbl_lnkHosttemplateToVariabledefinition','name','','',0,'',4),(28,'tbl_host','tbl_host','','parents','tbl_lnkHostToHost','host_name','','',0,'',2),(29,'tbl_host','tbl_hostgroup','','hostgroups','tbl_lnkHostToHostgroup','hostgroup_name','','',0,'',2),(30,'tbl_host','tbl_contactgroup','','contact_groups','tbl_lnkHostToContactgroup','contactgroup_name','','',0,'',2),(31,'tbl_host','tbl_contact','','contacts','tbl_lnkHostToContact','contact_name','','',0,'',2),(32,'tbl_host','tbl_timeperiod','','check_period','','timeperiod_name','','',0,'',1),(33,'tbl_host','tbl_command','','check_command','','command_name','','',0,'',1),(34,'tbl_host','tbl_timeperiod','','notification_period','','timeperiod_name','','',0,'',1),(35,'tbl_host','tbl_command','','event_handler','','command_name','','',0,'',1),(36,'tbl_host','tbl_hosttemplate','tbl_host','use_template','tbl_lnkHostToHosttemplate','template_name','name','',0,'',3),(37,'tbl_host','tbl_variabledefinition','','use_variables','tbl_lnkHostToVariabledefinition','name','','',0,'',4),(38,'tbl_hostgroup','tbl_host','','members','tbl_lnkHostgroupToHost','host_name','','',0,'',2),(39,'tbl_hostgroup','tbl_hostgroup','','hostgroup_members','tbl_lnkHostgroupToHostgroup','hostgroup_name','','',0,'',2),(40,'tbl_servicetemplate','tbl_host','','host_name','tbl_lnkServicetemplateToHost','host_name','','',0,'',2),(41,'tbl_servicetemplate','tbl_hostgroup','','hostgroup_name','tbl_lnkServicetemplateToHostgroup','hostgroup_name','','',0,'',2),(42,'tbl_servicetemplate','tbl_servicegroup','','servicegroups','tbl_lnkServicetemplateToServicegroup','servicegroup_name','','',0,'',2),(43,'tbl_servicetemplate','tbl_contactgroup','','contact_groups','tbl_lnkServicetemplateToContactgroup','contactgroup_name','','',0,'',2),(44,'tbl_servicetemplate','tbl_contact','','contacts','tbl_lnkServicetemplateToContact','contact_name','','',0,'',2),(45,'tbl_servicetemplate','tbl_timeperiod','','check_period','','timeperiod_name','','',0,'',1),(46,'tbl_servicetemplate','tbl_command','','check_command','','command_name','','',0,'',1),(47,'tbl_servicetemplate','tbl_timeperiod','','notification_period','','timeperiod_name','','',0,'',1),(48,'tbl_servicetemplate','tbl_command','','event_handler','','command_name','','',0,'',1),(49,'tbl_servicetemplate','tbl_servicetemplate','tbl_service','use_template','tbl_lnkServicetemplateToServicetemplate','template_name','name','',0,'',3),(50,'tbl_servicetemplate','tbl_variabledefinition','','use_variables','tbl_lnkServicetemplateToVariabledefinition','name','','',0,'',4),(51,'tbl_service','tbl_host','','host_name','tbl_lnkServiceToHost','host_name','','',0,'',2),(52,'tbl_service','tbl_hostgroup','','hostgroup_name','tbl_lnkServiceToHostgroup','hostgroup_name','','',0,'',2),(53,'tbl_service','tbl_servicegroup','','servicegroups','tbl_lnkServiceToServicegroup','servicegroup_name','','',0,'',2),(54,'tbl_service','tbl_contactgroup','','contact_groups','tbl_lnkServiceToContactgroup','contactgroup_name','','',0,'',2),(55,'tbl_service','tbl_contact','','contacts','tbl_lnkServiceToContact','contact_name','','',0,'',2),(56,'tbl_service','tbl_timeperiod','','check_period','','timeperiod_name','','',0,'',1),(57,'tbl_service','tbl_command','','check_command','','command_name','','',0,'',1),(58,'tbl_service','tbl_timeperiod','','notification_period','','timeperiod_name','','',0,'',1),(59,'tbl_service','tbl_command','','event_handler','','command_name','','',0,'',1),(60,'tbl_service','tbl_servicetemplate','tbl_service','use_template','tbl_lnkServiceToServicetemplate','template_name','name','',0,'',3),(61,'tbl_service','tbl_variabledefinition','','use_variables','tbl_lnkServiceToVariabledefinition','name','','',0,'',4),(62,'tbl_servicegroup','tbl_host','tbl_service','members','tbl_lnkServicegroupToService','host_name','service_description','',0,'',5),(63,'tbl_servicegroup','tbl_servicegroup','','servicegroup_members','tbl_lnkServicegroupToServicegroup','servicegroup_name','','',0,'',2),(64,'tbl_hostdependency','tbl_host','','dependent_host_name','tbl_lnkHostdependencyToHost_DH','host_name','','',0,'',2),(65,'tbl_hostdependency','tbl_host','','host_name','tbl_lnkHostdependencyToHost_H','host_name','','',0,'',2),(66,'tbl_hostdependency','tbl_hostgroup','','dependent_hostgroup_name','tbl_lnkHostdependencyToHostgroup_DH','hostgroup_name','','',0,'',2),(67,'tbl_hostdependency','tbl_hostgroup','','hostgroup_name','tbl_lnkHostdependencyToHostgroup_H','hostgroup_name','','',0,'',2),(68,'tbl_hostdependency','tbl_timeperiod','','dependency_period','','timeperiod_name','','',0,'',1),(69,'tbl_hostescalation','tbl_host','','host_name','tbl_lnkHostescalationToHost','host_name','','',0,'',2),(70,'tbl_hostescalation','tbl_hostgroup','','hostgroup_name','tbl_lnkHostescalationToHostgroup','hostgroup_name','','',0,'',2),(71,'tbl_hostescalation','tbl_contact','','contacts','tbl_lnkHostescalationToContact','contact_name','','',0,'',2),(72,'tbl_hostescalation','tbl_contactgroup','','contact_groups','tbl_lnkHostescalationToContactgroup','contactgroup_name','','',0,'',2),(73,'tbl_hostescalation','tbl_timeperiod','','escalation_period','','timeperiod_name','','',0,'',1),(74,'tbl_hostextinfo','tbl_host','','host_name','','host_name','','',0,'',1),(75,'tbl_servicedependency','tbl_host','','dependent_host_name','tbl_lnkServicedependencyToHost_DH','host_name','','',0,'',2),(76,'tbl_servicedependency','tbl_host','','host_name','tbl_lnkServicedependencyToHost_H','host_name','','',0,'',2),(77,'tbl_servicedependency','tbl_hostgroup','','dependent_hostgroup_name','tbl_lnkServicedependencyToHostgroup_DH','hostgroup_name','','',0,'',2),(78,'tbl_servicedependency','tbl_hostgroup','','hostgroup_name','tbl_lnkServicedependencyToHostgroup_H','hostgroup_name','','',0,'',2),(79,'tbl_servicedependency','tbl_service','','dependent_service_description','tbl_lnkServicedependencyToService_DS','service_description','','',0,'',6),(80,'tbl_servicedependency','tbl_service','','service_description','tbl_lnkServicedependencyToService_S','service_description','','',0,'',6),(81,'tbl_servicedependency','tbl_timeperiod','','dependency_period','','timeperiod_name','','',0,'',1),(82,'tbl_serviceescalation','tbl_host','','host_name','tbl_lnkServiceescalationToHost','host_name','','',0,'',2),(83,'tbl_serviceescalation','tbl_hostgroup','','hostgroup_name','tbl_lnkServiceescalationToHostgroup','hostgroup_name','','',0,'',2),(84,'tbl_serviceescalation','tbl_service','','service_description','tbl_lnkServiceescalationToService','service_description','','',0,'',6),(85,'tbl_serviceescalation','tbl_contact','','contacts','tbl_lnkServiceescalationToContact','contact_name','','',0,'',2),(86,'tbl_serviceescalation','tbl_contactgroup','','contact_groups','tbl_lnkServiceescalationToContactgroup','contactgroup_name','','',0,'',2),(87,'tbl_serviceescalation','tbl_timeperiod','','escalation_period','','timeperiod_name','','',0,'',1),(88,'tbl_serviceextinfo','tbl_host','','host_name','','host_name','','',0,'',1),(89,'tbl_serviceextinfo','tbl_service','','service_description','','service_description','','',0,'',1),(90,'tbl_command','tbl_lnkContacttemplateToCommandHost','','idSlave','','tbl_contacttemplate','','template_name',1,'0,0,0,1',0),(91,'tbl_command','tbl_lnkContacttemplateToCommandService','','idSlave','','tbl_contacttemplate','','template_name',1,'0,0,0,1',0),(92,'tbl_command','tbl_lnkContactToCommandHost','','idSlave','','tbl_contact','','contact_name',1,'1,1,0,1',0),(93,'tbl_command','tbl_lnkContactToCommandService','','idSlave','','tbl_contact','','contact_name',1,'1,1,0,1',0),(94,'tbl_command','tbl_host','','check_command','','','','host_name',1,'0,2,2,0',0),(95,'tbl_command','tbl_host','','event_handler','','','','host_name',1,'0,2,2,0',0),(96,'tbl_command','tbl_service','','check_command','','','','config_name,service_description',1,'1,1,2,0',0),(97,'tbl_command','tbl_service','','event_handler','','','','config_name,service_description',1,'0,2,2,0',0),(98,'tbl_contact','tbl_lnkContactgroupToContact','','idSlave','','tbl_contactgroup','','contactgroup_name',1,'1,2,0,1',0),(99,'tbl_contact','tbl_lnkContactToCommandHost','','idMaster','','tbl_command','','command_name',1,'0,0,0,1',0),(100,'tbl_contact','tbl_lnkContactToCommandService','','idMaster','','tbl_command','','command_name',1,'0,0,0,1',0),(101,'tbl_contact','tbl_lnkContactToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(102,'tbl_contact','tbl_lnkContactToContacttemplate','','idMaster','','tbl_contacttemplate','','template_name',1,'0,0,0,1',0),(103,'tbl_contact','tbl_lnkContactToVariabledefinition','','idMaster','','tbl_variabledefinition','','name',1,'0,0,0,2',0),(104,'tbl_contact','tbl_lnkHostescalationToContact','','idSlave','','tbl_hostescalation','','config_name',1,'1,1,0,1',0),(105,'tbl_contact','tbl_lnkHosttemplateToContact','','idSlave','','tbl_hosttemplate','','template_name',1,'0,0,0,1',0),(106,'tbl_contact','tbl_lnkHostToContact','','idSlave','','tbl_host','','host_name',1,'1,1,0,1',0),(107,'tbl_contact','tbl_lnkServiceescalationToContact','','idSlave','','tbl_serviceescalation','','config_name',1,'1,1,0,1',0),(108,'tbl_contact','tbl_lnkServicetemplateToContact','','idSlave','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(109,'tbl_contact','tbl_lnkServiceToContact','','idSlave','','tbl_service','','config_name,service_description',1,'1,1,0,1',0),(110,'tbl_contactgroup','tbl_lnkContactgroupToContact','','idMaster','','tbl_contact','','contact_name',1,'0,0,0,1',0),(111,'tbl_contactgroup','tbl_lnkContactgroupToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(112,'tbl_contactgroup','tbl_lnkContactgroupToContactgroup','','idSlave','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(113,'tbl_contactgroup','tbl_lnkContacttemplateToContactgroup','','idSlave','','tbl_contacttemplate','','template_name',1,'0,0,0,1',0),(114,'tbl_contactgroup','tbl_lnkContactToContactgroup','','idSlave','','tbl_contact','','contact_name',1,'0,0,0,1',0),(115,'tbl_contactgroup','tbl_lnkHostescalationToContactgroup','','idSlave','','tbl_hostescalation','','config_name',1,'1,1,0,1',0),(116,'tbl_contactgroup','tbl_lnkHosttemplateToContactgroup','','idSlave','','tbl_hosttemplate','','template_name',1,'0,0,0,1',0),(117,'tbl_contactgroup','tbl_lnkHostToContactgroup','','idSlave','','tbl_host','','host_name',1,'1,1,0,1',0),(118,'tbl_contactgroup','tbl_lnkServiceescalationToContactgroup','','idSlave','','tbl_serviceescalation','','config_name',1,'1,1,0,1',0),(119,'tbl_contactgroup','tbl_lnkServicetemplateToContactgroup','','idSlave','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(120,'tbl_contactgroup','tbl_lnkServiceToContactgroup','','idSlave','','tbl_service','','config_name,service_description',1,'1,1,0,1',0),(121,'tbl_contacttemplate','tbl_lnkContacttemplateToCommandHost','','idMaster','','tbl_command','','command_name',1,'0,0,0,1',0),(122,'tbl_contacttemplate','tbl_lnkContacttemplateToCommandService','','idMaster','','tbl_command','','command_name',1,'0,0,0,1',0),(123,'tbl_contacttemplate','tbl_lnkContacttemplateToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(124,'tbl_contacttemplate','tbl_lnkContacttemplateToContacttemplate','','idMaster','','tbl_contacttemplate','','template_name',1,'0,0,0,1',0),(125,'tbl_contacttemplate','tbl_lnkContacttemplateToContacttemplate','','idSlave','','tbl_contacttemplate','','template_name',1,'0,0,0,1',0),(126,'tbl_contacttemplate','tbl_lnkContacttemplateToVariabledefinition','','idMaster','','tbl_variabledefinition','','name',1,'0,0,0,2',0),(127,'tbl_contacttemplate','tbl_lnkContactToContacttemplate','','idSlave','','tbl_contact','','contact_name',1,'0,0,0,1',0),(128,'tbl_host','tbl_lnkHostdependencyToHost_DH','','idSlave','','tbl_hostdependency','','config_name',1,'1,1,0,1',0),(129,'tbl_host','tbl_lnkHostdependencyToHost_H','','idSlave','','tbl_hostdependency','','config_name',1,'1,1,0,1',0),(130,'tbl_host','tbl_lnkHostescalationToHost','','idSlave','','tbl_hostescalation','','config_name',1,'1,1,0,1',0),(131,'tbl_host','tbl_lnkHosttemplateToHost','','idSlave','','tbl_hosttemplate','','template_name',1,'0,0,0,1',0),(132,'tbl_host','tbl_lnkHostToContact','','idMaster','','tbl_contact','','contact_name',1,'0,0,0,1',0),(133,'tbl_host','tbl_lnkHostToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(134,'tbl_host','tbl_lnkHostToHost','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(135,'tbl_host','tbl_lnkHostToHost','','idSlave','','tbl_host','','host_name',1,'0,0,0,1',0),(136,'tbl_host','tbl_lnkHostToHostgroup','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(137,'tbl_host','tbl_lnkHostgroupToHost','','idSlave','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(138,'tbl_host','tbl_lnkHostToHosttemplate','','idMaster','','tbl_hosttemplate','','template_name',1,'0,0,0,1',0),(139,'tbl_host','tbl_lnkHostToVariabledefinition','','idMaster','','tbl_variabledefinition','','name',1,'0,0,0,2',0),(140,'tbl_host','tbl_lnkServicedependencyToHost_DH','','idSlave','','tbl_servicedependency','','config_name',1,'1,1,0,1',0),(141,'tbl_host','tbl_lnkServicedependencyToHost_H','','idSlave','','tbl_servicedependency','','config_name',1,'1,1,0,1',0),(142,'tbl_host','tbl_lnkServiceescalationToHost','','idSlave','','tbl_serviceescalation','','config_name',1,'1,1,0,1',0),(143,'tbl_host','tbl_lnkServicetemplateToHost','','idSlave','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(144,'tbl_host','tbl_lnkServiceToHost','','idSlave','','tbl_service','','config_name,service_description',1,'1,1,0,1',0),(145,'tbl_host','tbl_lnkServicegroupToService','','idSlaveH','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(146,'tbl_host','tbl_hostextinfo','','host_name','','','','host_name',1,'0,0,0,0',0),(147,'tbl_host','tbl_serviceextinfo','','host_name','','','','host_name',1,'0,0,0,0',0),(148,'tbl_hostdependency','tbl_lnkHostdependencyToHostgroup_DH','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(149,'tbl_hostdependency','tbl_lnkHostdependencyToHostgroup_H','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(150,'tbl_hostdependency','tbl_lnkHostdependencyToHost_DH','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(151,'tbl_hostdependency','tbl_lnkHostdependencyToHost_H','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(152,'tbl_hostescalation','tbl_lnkHostescalationToContact','','idMaster','','tbl_contact','','contact_name',1,'0,0,0,1',0),(153,'tbl_hostescalation','tbl_lnkHostescalationToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(154,'tbl_hostescalation','tbl_lnkHostescalationToHost','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(155,'tbl_hostescalation','tbl_lnkHostescalationToHostgroup','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(156,'tbl_hostgroup','tbl_lnkHostdependencyToHostgroup_DH','','idSlave','','tbl_hostdependency','','config_name',1,'0,0,0,1',0),(157,'tbl_hostgroup','tbl_lnkHostdependencyToHostgroup_H','','idSlave','','tbl_hostdependency','','config_name',1,'0,0,0,1',0),(158,'tbl_hostgroup','tbl_lnkHostescalationToHostgroup','','idSlave','','tbl_hostescalation','','config_name',1,'0,0,0,1',0),(159,'tbl_hostgroup','tbl_lnkHostgroupToHost','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(160,'tbl_hostgroup','tbl_lnkHostgroupToHostgroup','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(161,'tbl_hostgroup','tbl_lnkHostgroupToHostgroup','','idSlave','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(162,'tbl_hostgroup','tbl_lnkHosttemplateToHostgroup','','idSlave','','tbl_hosttemplate','','template_name',1,'0,0,0,1',0),(163,'tbl_hostgroup','tbl_lnkHostToHostgroup','','idSlave','','tbl_host','','host_name',1,'0,0,0,1',0),(164,'tbl_hostgroup','tbl_lnkServicedependencyToHostgroup_DH','','idSlave','','tbl_servicedependency','','config_name',1,'0,0,0,1',0),(165,'tbl_hostgroup','tbl_lnkServicedependencyToHostgroup_H','','idSlave','','tbl_servicedependency','','config_name',1,'0,0,0,1',0),(166,'tbl_hostgroup','tbl_lnkServiceescalationToHostgroup','','idSlave','','tbl_serviceescalation','','config_name',1,'0,0,0,1',0),(167,'tbl_hostgroup','tbl_lnkServicetemplateToHostgroup','','idSlave','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(168,'tbl_hostgroup','tbl_lnkServiceToHostgroup','','idSlave','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(169,'tbl_hostgroup','tbl_lnkServicegroupToService','','idSlaveHG','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(170,'tbl_hosttemplate','tbl_lnkHosttemplateToContact','','idMaster','','tbl_contact','','contact_name',1,'0,0,0,1',0),(171,'tbl_hosttemplate','tbl_lnkHosttemplateToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(172,'tbl_hosttemplate','tbl_lnkHosttemplateToHost','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(173,'tbl_hosttemplate','tbl_lnkHosttemplateToHostgroup','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(174,'tbl_hosttemplate','tbl_lnkHosttemplateToHosttemplate','','idMaster','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(175,'tbl_hosttemplate','tbl_lnkHosttemplateToHosttemplate','','idSlave','','tbl_hosttemplate','','template_name',1,'0,0,0,1',0),(176,'tbl_hosttemplate','tbl_lnkHosttemplateToVariabledefinition','','idMaster','','tbl_variabledefinition','','name',1,'0,0,0,2',0),(177,'tbl_hosttemplate','tbl_lnkHostToHosttemplate','','idSlave','','tbl_host','','host_name',1,'0,0,0,1',0),(178,'tbl_service','tbl_lnkServicedependencyToService_DS','','idSlave','','tbl_servicedependency','','config_name',1,'1,1,0,1',0),(179,'tbl_service','tbl_lnkServicedependencyToService_S','','idSlave','','tbl_servicedependency','','config_name',1,'1,1,0,1',0),(180,'tbl_service','tbl_lnkServiceescalationToService','','idSlave','','tbl_serviceescalation','','config_name',1,'1,1,0,1',0),(181,'tbl_service','tbl_lnkServicegroupToService','','idSlaveS','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(182,'tbl_service','tbl_lnkServiceToContact','','idMaster','','tbl_contact','','contact_name',1,'0,0,0,1',0),(183,'tbl_service','tbl_lnkServiceToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(184,'tbl_service','tbl_lnkServiceToHost','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(185,'tbl_service','tbl_lnkServiceToHostgroup','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(186,'tbl_service','tbl_lnkServiceToServicegroup','','idMaster','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(187,'tbl_service','tbl_lnkServiceToServicetemplate','','idMaster','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(188,'tbl_service','tbl_lnkServiceToVariabledefinition','','idMaster','','tbl_variabledefinition','','name',1,'0,0,0,2',0),(189,'tbl_service','tbl_serviceextinfo','','service_description','','','','host_name',1,'0,0,0,0',0),(190,'tbl_servicedependency','tbl_lnkServicedependencyToHostgroup_DH','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(191,'tbl_servicedependency','tbl_lnkServicedependencyToHostgroup_H','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(192,'tbl_servicedependency','tbl_lnkServicedependencyToHost_DH','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(193,'tbl_servicedependency','tbl_lnkServicedependencyToHost_H','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(194,'tbl_servicedependency','tbl_lnkServicedependencyToService_DS','','idMaster','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(195,'tbl_servicedependency','tbl_lnkServicedependencyToService_S','','idMaster','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(196,'tbl_serviceescalation','tbl_lnkServiceescalationToContact','','idMaster','','tbl_contact','','contact_name',1,'0,0,0,1',0),(197,'tbl_serviceescalation','tbl_lnkServiceescalationToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(198,'tbl_serviceescalation','tbl_lnkServiceescalationToHost','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(199,'tbl_serviceescalation','tbl_lnkServiceescalationToHostgroup','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(200,'tbl_serviceescalation','tbl_lnkServiceescalationToService','','idMaster','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(201,'tbl_servicegroup','tbl_lnkServicegroupToService','','idMaster','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(202,'tbl_servicegroup','tbl_lnkServicegroupToServicegroup','','idMaster','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(203,'tbl_servicegroup','tbl_lnkServicegroupToServicegroup','','idSlave','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(204,'tbl_servicegroup','tbl_lnkServicetemplateToServicegroup','','idSlave','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(205,'tbl_servicegroup','tbl_lnkServiceToServicegroup','','idSlave','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(206,'tbl_servicetemplate','tbl_lnkServicetemplateToContact','','idMaster','','tbl_contact','','contact_name',1,'0,0,0,1',0),(207,'tbl_servicetemplate','tbl_lnkServicetemplateToContactgroup','','idMaster','','tbl_contactgroup','','contactgroup_name',1,'0,0,0,1',0),(208,'tbl_servicetemplate','tbl_lnkServicetemplateToHost','','idMaster','','tbl_host','','host_name',1,'0,0,0,1',0),(209,'tbl_servicetemplate','tbl_lnkServicetemplateToHostgroup','','idMaster','','tbl_hostgroup','','hostgroup_name',1,'0,0,0,1',0),(210,'tbl_servicetemplate','tbl_lnkServicetemplateToServicegroup','','idMaster','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(211,'tbl_servicetemplate','tbl_lnkServicetemplateToServicetemplate','','idMaster','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(212,'tbl_servicetemplate','tbl_lnkServicetemplateToServicetemplate','','idSlave','','tbl_servicetemplate','','template_name',1,'0,0,0,1',0),(213,'tbl_servicetemplate','tbl_lnkServicetemplateToVariabledefinition','','idMaster','','tbl_variabledefinition','','name',1,'0,0,0,2',0),(214,'tbl_servicetemplate','tbl_lnkServiceToServicetemplate','','idSlave','','tbl_service','','config_name,service_description',1,'0,0,0,1',0),(215,'tbl_timeperiod','tbl_lnkTimeperiodToTimeperiod','','idMaster','','tbl_timeperiod','','timeperiod_name',1,'0,0,0,1',0),(216,'tbl_timeperiod','tbl_lnkTimeperiodToTimeperiod','','idSlave','','tbl_timeperiod','','timeperiod_name',1,'0,0,0,1',0),(217,'tbl_timeperiod','tbl_contact','','host_notification_period','','','','contact_name',1,'1,1,2,0',0),(218,'tbl_timeperiod','tbl_contact','','service_notification_period','','','','contact_name',1,'1,1,2,0',0),(219,'tbl_timeperiod','tbl_contacttemplate','','host_notification_period','','','','template_name',1,'0,2,2,0',0),(220,'tbl_timeperiod','tbl_contacttemplate','','service_notification_period','','','','template_name',1,'0,2,2,0',0),(221,'tbl_timeperiod','tbl_host','','check_period','','','','host_name',1,'1,1,2,0',0),(222,'tbl_timeperiod','tbl_host','','notification_period','','','','host_name',1,'1,1,2,0',0),(223,'tbl_timeperiod','tbl_hosttemplate','','check_period','','','','template_name',1,'0,2,2,0',0),(224,'tbl_timeperiod','tbl_hosttemplate','','notification_period','','','','template_name',1,'0,2,2,0',0),(225,'tbl_timeperiod','tbl_hostdependency','','dependency_period','','','','config_name',1,'0,2,2,0',0),(226,'tbl_timeperiod','tbl_hostescalation','','escalation_period','','','','config_name',1,'0,2,2,0',0),(227,'tbl_timeperiod','tbl_service','','check_period','','','','config_name,service_description',1,'1,1,2,0',0),(228,'tbl_timeperiod','tbl_service','','notification_period','','','','config_name,service_description',1,'0,2,2,0',0),(229,'tbl_timeperiod','tbl_servicetemplate','','check_period','','','','template_name',1,'0,2,2,0',0),(230,'tbl_timeperiod','tbl_servicetemplate','','notification_period','','','','template_name',1,'1,1,2,0',0),(231,'tbl_timeperiod','tbl_servicedependency','','dependency_period','','','','config_name',1,'0,2,2,0',0),(232,'tbl_timeperiod','tbl_serviceescalation','','escalation_period','','','','config_name',1,'0,2,2,0',0),(233,'tbl_timeperiod','tbl_timedefinition','','tipId','','','','id',1,'0,0,0,3',0),(234,'tbl_timeperiod','tbl_timeperiod','','use_template','tbl_lnkTimeperiodToTimeperiodUse','timeperiod_name','','',0,'',2),(235,'tbl_timeperiod','tbl_lnkTimeperiodToTimeperiodUse','','idMaster','','tbl_timeperiod','','timeperiod_name',1,'0,0,0,1',0),(236,'tbl_timeperiod','tbl_lnkTimeperiodToTimeperiodUse','','idSlave','','tbl_timeperiod','','timeperiod_name',1,'0,0,0,1',0),(237,'tbl_group','tbl_user','','users','tbl_lnkGroupToUser','username','','',0,'',0),(238,'tbl_group','tbl_lnkGroupToUser','','idMaster','','tbl_user','','username',1,'0,0,0,1',0),(239,'tbl_servicedependency','tbl_servicegroup','','dependent_servicegroup_name','tbl_lnkServicedependencyToServicegroup_DS','servicegroup_name','','',0,'',2),(240,'tbl_servicedependency','tbl_servicegroup','','servicegroup_name','tbl_lnkServicedependencyToServicegroup_S','servicegroup_name','','',0,'',2),(241,'tbl_servicedependency','tbl_lnkServicedependencyToServicegroup_DS','','idMaster','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(242,'tbl_servicedependency','tbl_lnkServicedependencyToServicegroup_S','','idMaster','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(243,'tbl_serviceescalation','tbl_servicegroup','','servicegroup_name','tbl_lnkServiceescalationToServicegroup','servicegroup_name','','',0,'',2),(244,'tbl_serviceescalation','tbl_lnkServiceescalationToServicegroup','','idMaster','','tbl_servicegroup','','servicegroup_name',1,'0,0,0,1',0),(245,'tbl_service','tbl_service','','parents','tbl_lnkServiceToService','service_description','','',0,'',7),(246,'tbl_servicetemplate','tbl_service','','parents','tbl_lnkServicetemplateToService','service_description','','',0,'',7);
UNLOCK TABLES;

--
-- Table structure for table `tbl_servicetemplate`
--

DROP TABLE IF EXISTS `tbl_servicetemplate`;
CREATE TABLE `tbl_servicetemplate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `host_name` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `host_name_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `hostgroup_name` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hostgroup_name_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `service_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parents` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `parents_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `importance` int(11) DEFAULT NULL,
  `servicegroups` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `servicegroups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `use_template` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_command` text COLLATE utf8_unicode_ci NOT NULL,
  `is_volatile` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `initial_state` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `max_check_attempts` int(11) DEFAULT NULL,
  `check_interval` int(11) DEFAULT NULL,
  `retry_interval` int(11) DEFAULT NULL,
  `active_checks_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `passive_checks_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_period` int(11) NOT NULL DEFAULT '0',
  `parallelize_check` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `obsess_over_service` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `check_freshness` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `freshness_threshold` int(11) DEFAULT NULL,
  `event_handler` int(11) NOT NULL DEFAULT '0',
  `event_handler_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `low_flap_threshold` int(11) DEFAULT NULL,
  `high_flap_threshold` int(11) DEFAULT NULL,
  `flap_detection_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `flap_detection_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `process_perf_data` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_status_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `retain_nonstatus_information` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `notification_interval` int(11) DEFAULT NULL,
  `first_notification_delay` int(11) DEFAULT NULL,
  `notification_period` int(11) NOT NULL DEFAULT '0',
  `notification_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notifications_enabled` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `contacts` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `contacts_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `contact_groups` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `contact_groups_tploptions` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `stalking_options` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `icon_image_alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_variables` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(8) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `import_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_name` (`template_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_servicetemplate`
--

LOCK TABLES `tbl_servicetemplate` WRITE;
INSERT INTO `tbl_servicetemplate` VALUES (1,'generic-service',0,2,0,2,'','',0,2,NULL,0,2,0,2,'0',0,'',3,10,2,1,1,1,1,1,0,NULL,0,1,NULL,NULL,1,'',1,1,1,60,NULL,1,'w,u,c,r',1,0,2,1,2,'','','','/pnp4nagios/graph?host=$HOSTNAME$&srv=$SERVICEDESC$','','',0,'0','1','2019-05-15 23:01:33',0,1,''),(2,'local-service',0,2,0,2,'','',0,2,NULL,0,2,1,2,'',2,'',4,5,1,2,2,0,2,2,2,NULL,0,2,NULL,NULL,2,'',2,2,2,NULL,NULL,0,'',2,0,2,0,2,'','','','','','',0,'0','1','2019-05-15 22:23:16',0,1,'');
UNLOCK TABLES;

--
-- Table structure for table `tbl_tablestatus`
--

DROP TABLE IF EXISTS `tbl_tablestatus`;
CREATE TABLE `tbl_tablestatus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tableName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domainId` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_tablestatus`
--

LOCK TABLES `tbl_tablestatus` WRITE;
INSERT INTO `tbl_tablestatus` VALUES (1,'tbl_command',1,'2019-05-15 22:53:11'),(2,'tbl_timeperiod',1,'2019-05-15 22:21:48'),(3,'tbl_contacttemplate',1,'2019-05-15 22:23:16'),(4,'tbl_hosttemplate',1,'2019-05-15 22:59:53'),(5,'tbl_servicetemplate',1,'2019-05-15 23:01:33'),(6,'tbl_contact',1,'2019-05-15 22:34:01'),(7,'tbl_contactgroup',1,'2019-05-15 22:34:01'),(8,'tbl_host',1,'2019-05-15 23:09:17'),(9,'tbl_hostgroup',1,'2019-05-15 23:09:17'),(10,'tbl_service',1,'2019-05-15 23:26:18');
UNLOCK TABLES;

--
-- Table structure for table `tbl_timedefinition`
--

DROP TABLE IF EXISTS `tbl_timedefinition`;
CREATE TABLE `tbl_timedefinition` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipId` int(10) unsigned NOT NULL,
  `definition` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `range` text COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_timedefinition`
--

LOCK TABLES `tbl_timedefinition` WRITE;
INSERT INTO `tbl_timedefinition` VALUES (1,1,'sunday','00:00-24:00','0000-00-00 00:00:00'),(2,1,'monday','00:00-24:00','0000-00-00 00:00:00'),(3,1,'tuesday','00:00-24:00','0000-00-00 00:00:00'),(4,1,'wednesday','00:00-24:00','0000-00-00 00:00:00'),(5,1,'thursday','00:00-24:00','0000-00-00 00:00:00'),(6,1,'friday','00:00-24:00','0000-00-00 00:00:00'),(7,1,'saturday','00:00-24:00','0000-00-00 00:00:00'),(8,2,'monday','09:00-17:00','0000-00-00 00:00:00'),(9,2,'tuesday','09:00-17:00','0000-00-00 00:00:00'),(10,2,'wednesday','09:00-17:00','0000-00-00 00:00:00'),(11,2,'thursday','09:00-17:00','0000-00-00 00:00:00'),(12,2,'friday','09:00-17:00','0000-00-00 00:00:00'),(13,4,'january 1','00:00-00:00','0000-00-00 00:00:00'),(14,4,'monday -1 may','00:00-00:00','0000-00-00 00:00:00'),(15,4,'july 4','00:00-00:00','0000-00-00 00:00:00'),(16,4,'monday 1 september','00:00-00:00','0000-00-00 00:00:00'),(17,4,'thursday 4 november','00:00-00:00','0000-00-00 00:00:00'),(18,4,'december 25','00:00-00:00','0000-00-00 00:00:00'),(19,5,'sunday','00:00-24:00','0000-00-00 00:00:00'),(20,5,'monday','00:00-24:00','0000-00-00 00:00:00'),(21,5,'tuesday','00:00-24:00','0000-00-00 00:00:00'),(22,5,'wednesday','00:00-24:00','0000-00-00 00:00:00'),(23,5,'thursday','00:00-24:00','0000-00-00 00:00:00'),(24,5,'friday','00:00-24:00','0000-00-00 00:00:00'),(25,5,'saturday','00:00-24:00','0000-00-00 00:00:00');
UNLOCK TABLES;

--
-- Table structure for table `tbl_timeperiod`
--

DROP TABLE IF EXISTS `tbl_timeperiod`;
CREATE TABLE `tbl_timeperiod` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timeperiod_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exclude` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_template` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `last_modified` datetime NOT NULL,
  `access_group` int(8) unsigned NOT NULL DEFAULT '0',
  `config_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `timeperiod_name` (`timeperiod_name`,`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_timeperiod`
--

LOCK TABLES `tbl_timeperiod` WRITE;
INSERT INTO `tbl_timeperiod` VALUES (1,'24x7','24 Hours A Day, 7 Days A Week',0,0,'24x7','1','1','2019-05-15 22:21:48',0,1),(2,'workhours','Normal Work Hours',0,0,'workhours','1','1','2019-05-15 22:21:48',0,1),(3,'none','No Time Is A Good Time',0,0,'none','1','1','2019-05-15 22:21:48',0,1),(4,'us-holidays','U.S. Holidays',0,0,'us-holidays','1','1','2019-05-15 22:21:48',0,1),(5,'24x7_sans_holidays','24x7 Sans Holidays',0,1,'24x7_sans_holidays','1','1','2019-05-15 22:21:48',0,1);
UNLOCK TABLES;
